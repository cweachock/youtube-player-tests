	/** Require.JS manifest
 * lists relative paths and shims for all JS libraries
 * so they can be loaded in a modular way at runtime, on demand
 */
var require = {
	//"baseUrl": "_cms",
	waitSeconds: 120,
	"paths": {
		/* jQuery */
		'jquery': [
			'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min',
			'lib/jquery-1.11.1.min'
		],
		"jquery.validate": [
			"//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min",
			"lib/jquery.validate"
		],

		/* Foundation */
		'foundation.core': 'lib/foundation/foundation',
		'foundation.abide': 'lib/foundation/foundation.abide',
		'foundation.alerts': 'lib/foundation/foundation.alerts',
		'foundation.clearing': 'lib/foundation/foundation.clearing',
		'foundation.cookie': 'lib/foundation/foundation.cookie',
		'foundation.dropdown': 'lib/foundation/foundation.dropdown',
		'foundation.forms': 'lib/foundation/foundation.forms',
		'foundation.interchange': 'lib/foundation/foundation.interchange',
		'foundation.joyride': 'lib/foundation/foundation.joyride',
		'foundation.magellan': 'lib/foundation/foundation.magellan',
		'foundation.orbit': 'lib/foundation/foundation.orbit',
		'foundation.placeholder': 'lib/foundation/foundation.placeholder',
		'foundation.reveal': 'lib/foundation/foundation.reveal',
		//'foundation.section': 'lib/foundation/foundation.section.custom', //TNF-2275 This version provides Accordion Deep Linking, but causes problem with Product Feature component
		'foundation.section': 'lib/foundation/foundation.section',
		'foundation.tooltips': 'lib/foundation/foundation.tooltips',
		'foundation.topbar': 'lib/foundation/foundation.topbar',

		/* 3rd Party Libraries */
		'base64': 'lib/base64',
		'cycle': 'lib/jquery.cycle2',
		'ddSlick': 'lib/ddSlick',
		"text" : "lib/require-text",
		"Mustache" : "lib/Mustache.min",
		'domReady': 'lib/domReady-2.0.1',
		'jquery-carousel': 'lib/jquery-carousel',
		'jquery.cookie': 'lib/jquery.cookie',
		'jquery.tooltipster': 'lib/jquery.tooltipster',
		"jquery-lazyload": "lib/jquery.lazyload",
		'jquery-mobile-events': 'lib/jquery-mobile-events',
		"jquery.spin": "lib/jquery.spin",
		'jquery-ui': [
			'//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min',
			'lib/jquery-ui-1.9.2.custom.min',
		],
		'fastclick': 'lib/fastclick',
		'matchmedia': 'lib/matchMedia-0.2.0',
		'modernizr': 'lib/modernizr-aem',
		"ooyala": "lib/jquery-ooyala",
		"owl.carousel.extended": "lib/owl.carousel.extended",
		'picturefill': 'lib/picturefill',
		'promise': 'lib/requirejs-promise',
		'pubsub': 'lib/jquery.pubsub',
		'pubsub.legacy-shim': 'lib/jquery.pubsub.legacy-shim',
		"spin": "lib/spin",
        
        
        //"videojs": "//vjs.zencdn.net/5.0/video.min", // cdn version provided by video.js
        "videojs": "lib/video-js-5.3.0/video.min",
        "videojs-player": "cms/videojs-player",
        "videojs-plugin-youtube": "lib/video-js-plugins/youtube.custom",

		/* CMS Scripts */
		'cms.accordion': 'cms/accordion',
		'cms.article-teaser-grid': 'cms/article-teaser-grid',
		'cms.productgrid': 'cms/productgrid',
		'cms.carousel-container':'cms/carousel-container',
		'cms.dynamicmediabanner':'cms/dynamicmediabanner',
		'cms.grid-readmore':'cms/grid-readmore',
		'cms.hovercaptions': 'cms/hovercaptions',
		'cms.iframeContainer':'cms/iframeContainer',
		'cms.iframeInner':'cms/iframeInner',
		'cms.thewall':'cms/thewall',
		'cms.subcatnavigation':'cms/subcatnavigation',
		'cms.secondarynavigation':'cms/secondarynavigation',
		'cms.content-spots':'cms/content-spots',
		'cms.product-carousel':'cms/product-carousel',
		'cms.verticaltabs':'cms/verticaltabs',
		'cms.productpicker':'cms/productpicker',
		'cms.productpickercontainer':'cms/productpickercontainer',
		'cms.videoplayer':'cms/videoplayer',
		'cms.searchresults':'cms/searchresults',
		'cms.signupform':'cms/signupform',
		'cms.teammembers':'cms/teammembers',
		'cms.article-readmore':'cms/article-readmore',
		'cms.promomodule':'cms/promomodule',
		
		/* WCS Scripts */
		"wcs.app": "wcs/app/App",
		"wcs.base-responsive-control": "wcs/app/BaseResponsiveControl",
		"wcs.breakpoint-set": "wcs/app/BreakpointSet",
		"wcs.config": "wcs/app/Config",
		"wcs.environment": "wcs/app/Environment",
		"wcs.global-navigation-slider": "wcs/app/GlobalNavigationSlider",
		"wcs.responsive": "wcs/app/Responsive",
		"wcs.secondary-nav": "wcs/app/SecondaryNav",

		"wcs.carousel-container" : "wcs/util/wcs.carousel-container",
		"wcs.selector-cache" : "wcs/util/selector-cache",
		"wcs.alert": "wcs/util/alert",
		"wcs.console": "wcs/util/console",
		"wcs.context": "wcs/util/context",
		"wcs.dom-ready": "wcs/util/dom-ready",
		"wcs.filters": "wcs/ui/filters",
		"wcs.form-validate": "wcs/util/form-validate",
		"wcs.validate-message": "wcs/util/msg-i18n",
		"wcs.global-events": "wcs/util/global-events",
		"wcs.hopup": "wcs/util/hopup",
		"wcs.popup": "wcs/util/popup-util",
		"wcs.html5-fallback": "wcs/util/html5FallBack",
		"wcs.img-swapper": "wcs/util/imgSwapper",
		"wcs.lang-msg": "wcs/util/lang-msg",
		"wcs.pgutil": "wcs/util/pgutil",
		"wcs.print-friendly": "wcs/util/printFriendly",
		"wcs.product-availability": "wcs/util/product-availability",
		"wcs.net": "wcs/util/net",
		"wcs.text": "wcs/util/text",
		"wcs.utils": "wcs/util/utils",
		"wcs.video-player": "wcs/util/videoPlayer",
		"wcs.tooltip" : "wcs/util/tooltip-util",

		"wcs.ajax-dialog": "wcs/ui/ajax-dialog",
		"wcs.comparables": "wcs/ui/comparables",
		"wcs.product-grid": "wcs/ui/product-grid",
		"wcs.refresh-area": "wcs/ui/refresh-area",
		"wcs.teaser": "wcs/ui/teaser",
		"wcs.topbar-carousel": "wcs/ui/topbar-carousel",
		"wcs.promo-carousel": "wcs/ui/promo-carousel",

		"wcs.address-book": "wcs/view/address-book",
		"wcs.address-editor": "wcs/view/address-editor",
		"wcs.availability-notify": "wcs/view/availability-notify",
		"wcs.product-price": "wcs/view/product-price",
		"wcs.cart-totals": "wcs/view/cart-totals",
		"wcs.cc-profile": "wcs/view/cc-profile",
		"wcs.checkout-loyalty": "wcs/view/checkout-loyalty",
		"wcs.compare-view": "wcs/view/compare-view",
		"wcs.credit-cards": "wcs/view/credit-cards",
		"wcs.global-nav": "wcs/view/global-nav",
		"wcs.logon-register-view": "wcs/view/logon-register-view", 
		"wcs.order-history": "wcs/view/order-history",
		"wcs.product-context": "wcs/view/product-context",
		"wcs.product-list-view": "wcs/view/product-list-view",

		"wcs.product-form": "wcs/view/product-form/product-form",
		"wcs.product-form-core": "wcs/view/product-form/product-form-core-logic",
		"wcs.product-form-util": "wcs/view/product-form/product-form-utilities",
		"wcs.product-form-context": "wcs/view/product-form/product-form-context-handler",
		"wcs.product-form-drawer": "wcs/view/product-form/product-form-drawer",
		"wcs.product-form-events": "wcs/view/product-form/product-form-events",
		"wcs.product-form-notify": "wcs/view/product-form/product-form-notify",
		"wcs.product-form-online-dealers": "wcs/view/product-form/product-form-online-dealers",
		"wcs.product-form-detail": "wcs/view/product-form/product-form-detail",

		"wcs.product-views": "wcs/view/product-views/product-views",
		"wcs.product-views-util" : "wcs/view/product-views/product-view-utilities",
		"wcs.product-views-swatches" : "wcs/view/product-views/product-view-swatches",
		"wcs.product-views-s7" : "wcs/view/product-views/product-view-s7",
		"wcs.product-views-olapic" : "wcs/view/product-views/product-view-olapic",
		"wcs.product-views-equipment-compare" : "wcs/view/product-views/product-view-equipment-compare",		

		"wcs.youtube-video" : "wcs/view/product-views/pdp-videos/pdp-videos-youtube",
		
		"wcs.find-in-store" : "wcs/view/find-in-store/find-in-store",
		"wcs.find-in-store-on-page" : "wcs/view/find-in-store/find-in-store-on-page",
		"wcs.find-in-store-in-popup" : "wcs/view/find-in-store/find-in-store-in-popup",
		"wcs.find-in-store-util" : "wcs/view/find-in-store/find-in-store-util",
		"wcs.find-in-store-model" : "wcs/view/find-in-store/find-in-store-model",
		"wcs.find-in-store-alt-colors" : "wcs/view/find-in-store/find-in-store-alt-colors",
		"wcs.find-in-store-render" : "wcs/view/find-in-store/find-in-store-render",

		"wcs.profile": "wcs/view/profile",
		"wcs.mini-cart": "wcs/view/mini-cart",
		"wcs.mini-list": "wcs/view/mini-list",
		"wcs.right-panel": "wcs/view/right-panel",
		"wcs.top-panel": "wcs/view/top-panel",
		"wcs.mobile-panel": "wcs/view/mobile-panel",
		"wcs.my-wishlist": "wcs/view/my-wishlist",
		"wcs.track-orders": "wcs/view/track-orders",
		"wcs.search-autocomplete": "wcs/view/search-autocomplete",
		"wcs.s7-viewer": "wcs/view/s7-viewer",
		"wcs.s7-viewer-factory": "wcs/view/s7-viewer-factory",
		"wcs.wishlist-share": "wcs/view/wishlist-share",
		"wcs.bundle-view-alt": "wcs/view/product-bundle-views-alt",
		"wcs.checkout-gift-options-multi-item": "wcs/view/checkout-gift-option/checkout-gift-option-multi-item",
		"wcs.checkout-gift-options-shipping": "wcs/view/checkout-gift-option/checkout-gift-option-shipping",
		"wcs.checkout-gift-options-setup": "wcs/view/checkout-gift-option/checkout-gift-option-setup",
		"wcs.checkout-gift-options-single-item": "wcs/view/checkout-gift-option/checkout-gift-option-single-item",
		"wcs.checkout-gift-options-utils": "wcs/view/checkout-gift-option/checkout-gift-option-utils",
		"wcs.checkout-gift-option": "wcs/view/checkout-gift-option/checkout-gift-option",
		"wcs.free-gift": "wcs/view/free-gift",

		"wcs.custom-shoes": "wcs/custom-shoes",
		"wcs.quick-view": "wcs/quick-view",

		"wcs.checkout-email-signup": "wcs/view/checkout-email-signup",

		"wcs.checkout-leave": "wcs/checkout-change-tracker",
		"wcs.product-schema": "wcs/ui/product-schema",

		"wcs.zmetrics": "metrics/zmetrics",
        "wcs.cm-listener": "metrics/cm-listener",
        "wcs.ga-listener": "metrics/ga-listener",
        "wcs.gtm-listener": "metrics/gtm-listener",
        "cm.eluminate": "//libs.coremetrics.com/eluminate",
        "ga.ga": "//www.google-analytics.com/ga.js",

		/* Shared Scripts across apps */
		'shared.lazyload': 'shared/lazy-load',
		'shared.wheretogetit':'shared/where-to-get-it',
		'shared.equalizer':'shared/equalizer',
		'shared.footer':'cms/footer',
	},
	map: {
		"*": {"pubsub": "pubsub.legacy-shim"},
		"pubsub.legacy-shim": {"pubsub": "pubsub"}
	},
	"shim": {
		/* Foundation */
		'foundation.core': {
			deps: [
				'jquery',
				'modernizr',
				'matchmedia' // VFDP-637 Leverage newer matchMedia polyfill for better IE9 performance
			],
			exports: 'Foundation'
		},
		'foundation.abide':			['foundation.core'],
		'foundation.alerts':		['foundation.core'],
		'foundation.clearing':		['foundation.core'],
		'foundation.cookie':		['foundation.core'],
		'foundation.dropdown':		['foundation.core'],
		'foundation.forms':			['foundation.core'],
		'foundation.interchange':	['foundation.core'],
		'foundation.joyride':		['foundation.core','foundation.cookie'],
		'foundation.magellan':		['foundation.core'],
		'foundation.orbit':			['foundation.core'],
		'foundation.placeholder':	{
			deps: ['foundation.core'], // TODO foundation.core probably isn't necessary for placeholder
			exports: 'Placeholder'
		},
		'foundation.reveal':		['foundation.core'],
		'foundation.section':		['foundation.core'],
		'foundation.tooltips':		['foundation.core'],
		'foundation.topbar':		['foundation.core'],

		/* lib Scripts */
		'cycle': {
			deps: [
			'jquery'
			]
		},
		'ddSlick': {
			deps: [
			'jquery'
			]
		},
		'jquery.cookie': {
			deps: [
			'jquery'
			]
		},
		'jquery-lazyload':{
			deps: ['jquery']
		},
		'jquery-mobile-events': ['jquery'],
		'jquery.tooltipster': {
			deps: [
			'jquery'
			]
		},
		"jquery.validate": {
			deps: ["jquery"]
		},
		"wcs.validate-message": {
			deps: ["jquery.validate"]
		},
		'jquery-ui': {
			deps: [
			'jquery'
			]
		},
		'fastclick': {
			exports: 'FastClick'
		},
		'modernizr': {
			exports: 'Modernizr'
		},
		"ooyala":{
			deps: ["jquery"]
		},
		"owl.carousel.extended": {
			deps: ["jquery"]
		},
		'picturefill': {
			exports: 'picturefill'
		},
		/**
		 * Custom Scripts for CMS features
		 * Not needed if handled via a define() in the target script
		 **/

		// 'example': {
		//	deps: [
		//	'jquery'
		//	]
		// },

		/**
		 * Custom Scripts for WCS features
		 * Not needed if handled via a define() in the target script
		 **/
		"wcs.custom-shoes":{
			deps: ["jquery", "jquery-ui", "pubsub"]
		},
		"wcs.cm-listener": {
			deps: ["cm.eluminate"]
		},
		"wcs.ga-listener": {
			deps: ["ga.ga"]
		},
		"jquery-carousel": ["jquery","jquery-mobile-events","jquery-ui"],
		"wcs.topbar-carousel":{
			deps: ["jquery-carousel","foundation.core"]
		}
	},
};