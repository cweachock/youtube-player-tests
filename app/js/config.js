(function(){
	'use strict'; 
    require.config({ 
        config: { 
            app: { 
                files: ['main'] 
            },
            
        } 
    }); 

    //- See more at: https://vickev.com/#!/article/grunt-the-perfect-tool-for-require-js
    // Expose to the rest of the world
    // if (typeof module !== 'undefined') {
    //     module.exports = config;
    // }
    // else if (typeof require.config !== 'undefined') {
    //     require.config(config);
    // }
})();