/** Require.JS manifest
 * lists relative paths and shims for all JS libraries
 * so they can be loaded in a modular way at runtime, on demand
 */
var require = {
	// "baseUrl": "@@JSLIBPATH",
	waitSeconds: 120,
	"paths": {
		/* jQuery */
		'jquery': [
			'//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min',
			'lib/jquery-1.11.1.min'
		],
		"jquery.validate": [
			"//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min",
			"lib/jquery.validate"
		],
		'jquery-mobile-events': [
			"//cdnjs.cloudflare.com/ajax/libs/jquery-touch-events/1.0.5/jquery.mobile-events.min",
			'lib/jquery-mobile-events-1.0.5'
		],
		"jquery-caret": "lib/jquery.caret.1.02.min",
		"jquery-textchange": "lib/jquery.splendid.textchange",

		/* Foundation */
		'foundation.core': 'lib/foundation/foundation',
		'foundation.abide': 'lib/foundation/foundation.abide',
		'foundation.alerts': 'lib/foundation/foundation.alerts',
		'foundation.clearing': 'lib/foundation/foundation.clearing',
		'foundation.cookie': 'lib/foundation/foundation.cookie',
		'foundation.dropdown': 'lib/foundation/foundation.dropdown',
		'foundation.forms': 'lib/foundation/foundation.forms',
		'foundation.interchange': 'lib/foundation/foundation.interchange',
		'foundation.joyride': 'lib/foundation/foundation.joyride',
		'foundation.magellan': 'lib/foundation/foundation.magellan',
		'foundation.orbit': 'lib/foundation/foundation.orbit',
		'foundation.placeholder': 'lib/foundation/foundation.placeholder',
		'foundation.reveal': 'lib/foundation/foundation.reveal',
		//'foundation.section': 'lib/foundation/foundation.section.custom', //TNF-2275 This version provides Accordion Deep Linking, but causes problem with Product Feature component
		'foundation.section': 'lib/foundation/foundation.section',
		'foundation.tooltips': 'lib/foundation/foundation.tooltips',
		'foundation.topbar': 'lib/foundation/foundation.topbar',

		/* 3rd Party Libraries */
		'base64': 'lib/base64',
		'cycle': 'lib/jquery.cycle2',
		'ddSlick': 'lib/ddSlick',
		"text" : "lib/require-text",
		"Mustache" : "lib/Mustache.min",
		"Handlebars" : "lib/handlebars-4.0.5",
		'domReady': 'lib/domReady-2.0.1',
		'jquery-carousel': 'lib/jquery-carousel',
		'jquery.XDomainRequest': 'lib/jQuery.XDomainRequest',
		'jquery.cookie': 'lib/jquery.cookie',
		'jquery.tooltipster': 'lib/jquery.tooltipster',
		"jquery-lazyload": "lib/jquery.lazyload",
		"jquery.spin": "lib/jquery.spin",
		"jquery-bridget" : "lib/jquery-bridget",
		'jquery-ui': [
			'//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min',
			'lib/jquery-ui-1.9.2.custom.min',
		],
		"jquery.hoverintent" : "lib/hover.intent",
		'fastclick': 'lib/fastclick-1.0.6.min',
		'matchmedia': 'lib/matchMedia-0.2.0',
		'modernizr': 'lib/modernizr-aem',
		"Clipboard": "lib/clipboard.min",
		"ooyala": "lib/jquery-ooyala",
		"owl.carousel.extended": "lib/owl.carousel.extended",
		'picturefill': 'lib/picturefill',
		'promise': 'lib/requirejs-promise',
		'pubsub': 'lib/jquery.pubsub.custom',
		"spin": "lib/spin",
		"isotope" : "lib/isotope.min",
		"cropper" : "lib/cropper.min",
		"preloadjs": "lib/preloadjs-0.6.2.min",

        /* 3D configurator Libraries */
		"Detector": "lib/Detector",
        'three': 'lib/three.min',
        'Tween': 'lib/Tween',
        'jqTabs': 'lib/jqTabs',
        'OrbitControls': 'lib/OrbitControls',
        'tinycolor': 'lib/tinycolor.min',
        'configurator.main': 'configurator/configurator',
        'configurator-app': 'configurator/configurator-app',
        'configurator-buyflow': 'configurator/configurator-buyflow',
		'configurator-design-inspiration': 'configurator/configurator-design-inspiration',
		'configurator-detection': 'configurator/configurator-detection',
        'configurator-env': 'configurator/configurator-env',
        'configurator-events': 'configurator/configurator-events',
        'configurator-modals': 'configurator/configurator-modals',
        'configurator-model-customizer': 'configurator/configurator-model-customizer',
        'configurator-model-setup': 'configurator/configurator-model-setup',
        'configurator-personalization': 'configurator/configurator-personalization',
        'configurator-preloader': 'configurator/configurator-preloader',
        'configurator-scene-setup': 'configurator/configurator-scene-setup',
        'configurator-swatches-ui': 'configurator/configurator-swatches-ui',
        'configurator-ui': 'configurator/configurator-ui',
        'configurator-utils': 'configurator/configurator-utils',
        'configurator-ugc': 'configurator/configurator-ugc',
        'configurator-commerce': 'configurator/configurator-commerce',
        'configurator-image-service': 'configurator/configurator-image-service',
        'configurator2d': 'configurator/configurator2d',
        'configurator-sharing': 'configurator/configurator-sharing',
        'configurator-component-geo-states': 'configurator/configurator-component-geo-states',


        //"videojs": "//vjs.zencdn.net/5.0/video.min", // cdn version provided by video.js
        "videojs": "lib/video-js-5.3.0/video.min",
        "videojs-player": "cms/videojs-player",
        "videojs-plugin-youtube": "lib/video-js-plugins/youtube.custom",

		/* CMS Scripts */
		'cms.accordion': 'cms/accordion',
		'cms.article-teaser-grid': 'cms/article-teaser-grid',
		'cms.productgrid': 'cms/productgrid',
		'cms.carousel-container':'cms/carousel-container',
		'cms.dynamicmediabanner':'cms/dynamicmediabanner',
		'cms.grid-readmore':'cms/grid-readmore',
		'cms.hovercaptions': 'cms/hovercaptions',
		'cms.iframeInner':'cms/iframeInner',
		'cms.image-with-hotspots':'cms/image-with-hotspots',
		'cms.thewall':'cms/thewall',
		'cms.subcatnavigation':'cms/subcatnavigation',
		'cms.secondarynavigation':'cms/secondarynavigation',
		'cms.product-carousel':'cms/product-carousel',
		'cms.verticaltabs':'cms/verticaltabs',
		'cms.productpicker':'cms/productpicker',
		'cms.productpickercontainer':'cms/productpickercontainer',
		'cms.videoplayer':'cms/videoplayer',
		'cms.searchresults':'cms/searchresults',
		'cms.signupform':'cms/signupform',
		'cms.teammembers':'cms/teammembers',
		'cms.article-readmore':'cms/article-readmore',
		'cms.promomodule':'cms/promomodule',
		'cms.styleselector':'cms/style_selector',
		'cms.joyride-component': 'cms/joyride-component',
		'cms.utilityNav': 'cms/utilityNav',
		/* WCS Scripts */
		"wcs.app": "wcs/app/App",
		"wcs.base-responsive-control": "wcs/app/BaseResponsiveControl",
		"wcs.breakpoint-set": "wcs/app/BreakpointSet",
		"wcs.config": "wcs/app/Config",
		"wcs.environment": "wcs/app/Environment",
		"wcs.responsive": "wcs/app/Responsive",
		"wcs.secondary-nav": "wcs/app/SecondaryNav",

		"wcs.topbar-core": "wcs/app/topbar/topbar-core",
		"wcs.topbar-helper": "wcs/app/topbar/topbar-helper",
		"wcs.topbar-search": "wcs/app/topbar/topbar-search",
		"wcs.topbar-util": "wcs/app/topbar/topbar-util",
		"wcs.topbar-slider": "wcs/app/topbar/topbar-slider",
		"menu-accordion": "wcs/app/topbar/menu-accordion",

		"wcs.carousel-container" : "wcs/util/wcs.carousel-container",
		"wcs.selector-cache" : "wcs/util/selector-cache",
		"wcs.alert": "wcs/util/alert",
		"wcs.console": "wcs/util/console",
		"wcs.context": "wcs/util/context",
		"wcs.filters": "wcs/ui/filters",
		"wcs.form-validate": "wcs/util/form-validate",
		"wcs.validate-message": "wcs/util/msg-i18n",
		"wcs.global-events": "wcs/util/global-events",
		"wcs.hopup": "wcs/util/hopup",
		"wcs.popup": "wcs/util/popup-util",
		"wcs.html5-fallback": "wcs/util/html5FallBack",
		"wcs.img-swapper": "wcs/util/imgSwapper",
		"wcs.lang-msg": "wcs/util/lang-msg",
		"wcs.pgutil": "wcs/util/pgutil",
		"wcs.print-friendly": "wcs/util/printFriendly",
		"wcs.product-availability": "wcs/util/product-availability",
		"wcs.net": "wcs/util/net",
		"wcs.text": "wcs/util/text",
		"wcs.utils": "wcs/util/utils",
		"wcs.video-player": "wcs/util/videoPlayer",
		"wcs.tooltip" : "wcs/util/tooltip-util",
		"wcs.util.ajax-form": "wcs/util/ajax-form",
		"wcs.util.url-query-string": "wcs/util/url-query-string",

		"wcs.comparables": "wcs/ui/comparables",
		"wcs.product-grid": "wcs/ui/product-grid",
		"wcs.teaser": "wcs/ui/teaser",
		"wcs.topbar-carousel": "wcs/ui/topbar-carousel",
		"wcs.promo-carousel": "wcs/ui/promo-carousel",
		"wcs.single-pass": "wcs/ui/single-pass",

		"my-account" : "wcs/my-account",
		"wcs.address-book": "wcs/view/address-book",
		"wcs.address-editor": "wcs/view/address-editor",
		"wcs.availability-notify": "wcs/view/availability-notify",
		"wcs.product-price": "wcs/view/product-price",
		"wcs.cart-totals": "wcs/view/cart-totals",
		"wcs.cc-profile": "wcs/view/cc-profile",
		"wcs.reset-pw-cancel-button": "wcs/view/reset-pw-cancel-button",
		"wcs.checkout-loyalty": "wcs/view/checkout-loyalty",
		"wcs.checkout-header-cookie": "wcs/checkout-header-cookie",
		"wcs.compare-view": "wcs/view/compare-view",
		"wcs.credit-cards": "wcs/view/credit-cards",
		"wcs.global-nav": "wcs/view/global-nav",
		"wcs.logon-register-view": "wcs/view/logon-register-view",
		"wcs.order-history": "wcs/view/order-history",
		"wcs.product-context": "wcs/view/product-context",
		"wcs.product-list-view": "wcs/view/product-list-view",
		"wcs.product-list-alt-views": "wcs/view/product-list-alt-views",
		"wcs.product-list-view-floating-filters": "wcs/view/plp-floating-filters",

		"wcs.product-form": "wcs/view/product-form/product-form",
		"wcs.product-form-core": "wcs/view/product-form/product-form-core-logic",
		"wcs.product-form-util": "wcs/view/product-form/product-form-utilities",
		"wcs.product-form-context": "wcs/view/product-form/product-form-context-handler",
		"wcs.product-form-drawer": "wcs/view/product-form/product-form-drawer",
		"wcs.product-form-pdpswatches-events": "wcs/view/product-form/product-form-pdpswatches-events",
		"wcs.product-form-notify": "wcs/view/product-form/product-form-notify",
		"wcs.product-form-online-dealers": "wcs/view/product-form/product-form-online-dealers",
		"wcs.product-form-detail": "wcs/view/product-form/product-form-detail",
		"wcs.product-form-size-chart-container" : "wcs/view/product-form/product-form-size-chart-container",
		"wcs.product-form-update-item" : "wcs/view/product-form/product-form-update-item",
		"wcs.product-form-exchange-item" : "wcs/view/product-form/product-form-exchange-item",
		"wcs.product-form-add-to-cart" : "wcs/view/product-form/product-form-add-to-cart",
		"wcs.product-form-colorway-events" : "wcs/view/product-form/product-form-colorway-events",
		"wcs.product-form-html5history": "wcs/view/product-form/product-form-html5history",

		"wcs.product-views": "wcs/view/product-views/product-views",
		"wcs.product-views-util" : "wcs/view/product-views/product-view-utilities",
		"wcs.product-views-swatches" : "wcs/view/product-views/product-view-swatches",
		"wcs.product-views-s7" : "wcs/view/product-views/product-view-s7",
		"wcs.product-views-olapic" : "wcs/view/product-views/product-view-olapic",
		"wcs.product-views-equipment-compare" : "wcs/view/product-views/product-view-equipment-compare",

		"wcs.product-video-controller" : "wcs/view/product-views/pdp-videos/pdp-video-controller",
		"wcs.product-video-plugin" : "wcs/view/product-views/pdp-videos/pdp-video-plugin",

		"wcs.find-in-store" : "wcs/view/find-in-store/find-in-store",
		"wcs.find-in-store-util" : "wcs/view/find-in-store/find-in-store-util",
		"wcs.find-in-store-model" : "wcs/view/find-in-store/find-in-store-model",
		"wcs.find-in-store-alt-colors" : "wcs/view/find-in-store/find-in-store-alt-colors",
		"wcs.find-in-store-render" : "wcs/view/find-in-store/find-in-store-render",

		"wcs.bundle-view-alt": "wcs/view/shop-the-look/product-bundle-views-alt",
		"wcs.product-bundle-views": "wcs/view/shop-the-look/product-bundle-views",

		"wcs.profile": "wcs/view/profile",
		"wcs.mini-cart": "wcs/view/mini-cart",
		"wcs.mini-list": "wcs/view/mini-list",
		"wcs.right-panel": "wcs/view/right-panel",
		"wcs.top-panel": "wcs/view/top-panel",
		"wcs.mobile-panel": "wcs/view/mobile-panel",
		"wcs.my-wishlist": "wcs/view/my-wishlist",
		"wcs.track-orders": "wcs/view/track-orders",
		"wcs.google-recaptcha": "wcs/google/recaptcha",
		"wcs.google-giftcard-recaptcha": "wcs/google/giftcard-recaptcha",
		"wcs.google-creditcard-recaptcha": "wcs/google/creditcard-recaptcha",
		"wcs.s7-viewer": "wcs/view/s7-viewer",
		"wcs.s7-viewer-factory": "wcs/view/s7-viewer-factory",
		"wcs.wishlist-share": "wcs/view/wishlist-share",

		"wcs.checkout-gift-options-multi-item": "wcs/view/checkout-gift-option/checkout-gift-option-multi-item",
		"wcs.checkout-gift-options-shipping": "wcs/view/checkout-gift-option/checkout-gift-option-shipping",
		"wcs.checkout-gift-options-setup": "wcs/view/checkout-gift-option/checkout-gift-option-setup",
		"wcs.checkout-gift-options-single-item": "wcs/view/checkout-gift-option/checkout-gift-option-single-item",
		"wcs.checkout-gift-options-utils": "wcs/view/checkout-gift-option/checkout-gift-option-utils",
		"wcs.checkout-gift-option": "wcs/view/checkout-gift-option/checkout-gift-option",
		"wcs.free-gift": "wcs/view/free-gift",
		"wcs.newsletter-email-subscription": "wcs/view/newsletter-email-subscription",
		"wcs.custom-shoes": "wcs/custom-shoes",
		"wcs.quick-view": "wcs/quick-view",

		"wcs.checkout-email-signup": "wcs/view/checkout-email-signup",
		"wcs.validate-tnc": "wcs/view/validate-tnc",
		"wcs.checkout-leave": "wcs/checkout-change-tracker",
		"wcs.order-confirmation": "wcs/order-confirmation",
		"wcs.shipping-delivery-estimate" : "wcs/checkout-shipping-delivery-estimate",
		"wcs.collection-form": "wcs/view/collection-form",
		"wcs.applepay-button":"wcs/view/applepay",
		
		"wcs.zmetrics": "metrics/zmetrics",
        "wcs.cm-listener": "metrics/cm-listener",
        "wcs.gtm-listener": "metrics/gtm-listener",
        "wcs.cms-content-lazy-load": "wcs/util/cms-content-lazy-load",
        "cm.eluminate": "//libs.coremetrics.com/eluminate",

		"datalayer.global":						"metrics/datalayer/global",
        "datalayer.wcs.account":				"metrics/datalayer/wcs.account",
        "datalayer.wcs.checkout-step":			"metrics/datalayer/wcs.checkout-step",
        "datalayer.wcs.mini-list":				"metrics/datalayer/wcs.mini-list",
        "datalayer.wcs.my-account":				"metrics/datalayer/wcs.my-account",
        "datalayer.wcs.my-wishlist":			"metrics/datalayer/wcs.my-wishlist",
        "datalayer.wcs.order-confirmation":		"metrics/datalayer/wcs.order-confirmation",
        "datalayer.wcs.product-form":			"metrics/datalayer/wcs.product-form",
        "datalayer.wcs.product-list":			"metrics/datalayer/wcs.product-list",

        // Customs configurator
        "datalayer.cms.configurator":			"metrics/datalayer/cms.configurator",

		// YouTube Events
		"datalayer.youtube": "metrics/datalayer/youtube",

        "coremetrics.wcs.checkout-logon":		"metrics/coremetrics/wcs.checkout-logon",
        "coremetrics.wcs.checkout-step":		"metrics/coremetrics/wcs.checkout-step",
        "coremetrics.wcs.gift-option":			"metrics/coremetrics/wcs.gift-option",
        "coremetrics.wcs.mini-list":			"metrics/coremetrics/wcs.mini-list",
        "coremetrics.wcs.my-account":			"metrics/coremetrics/wcs.my-account",
        "coremetrics.wcs.order-confirmation":	"metrics/coremetrics/wcs.order-confirmation",
        "coremetrics.wcs.product-detail":		"metrics/coremetrics/wcs.product-detail",
        "coremetrics.wcs.shopcart-items":		"metrics/coremetrics/wcs.shopcart-items",
        "coremetrics.wcs.product-form":			"metrics/coremetrics/wcs.product-form",

		/* Shared Scripts across apps */
		'shared.lazyload': 'shared/lazy-load',
		'shared.wheretogetit':'shared/where-to-get-it',
		'shared.equalizer':'shared/equalizer',
		'shared.kiosk-button': 'shared/kiosk-button',
    'shared.kiosk': 'shared/kiosk/global',
		'shared.footer':'cms/footer',
		'shared.ajax-hopup-modal': 'shared/ajax-hopup-modal',
		'shared.range-slider': 'shared/range-slider',

		'vfdp-s7-viewer.app': 'wcs/view/vfdp-s7-viewer/app',
		'vfdp-s7-viewer.config': 'wcs/view/vfdp-s7-viewer/config',
		'vfdp-s7-viewer.helpers': 'wcs/view/vfdp-s7-viewer/helpers',
		'vfdp-s7-viewer.viewer.prototype': 'wcs/view/vfdp-s7-viewer/viewer-prototype',
		'vfdp-s7-viewer.zoomviewer': 'wcs/view/vfdp-s7-viewer/zoomviewer',
		'vfdp-s7-viewer.flyoutzoomviewer': 'wcs/view/vfdp-s7-viewer/flyoutzoomviewer',
		'vfdp-s7-viewer.videoplayer': 'wcs/view/vfdp-s7-viewer/videoplayer',
		'vfdp-s7-viewer.sdk': (typeof SCENE7 !== 'undefined' && SCENE7.hasOwnProperty('HOST') ? SCENE7.HOST : "//s7d1.scene7.com/") + "s7sdk/3.0/js/s7sdk/utils/Utils",

		'pdp-social-share.app': "wcs/view/pdp-social-share",

		// shared handlebars components
		'handlebars-loader' : 'shared/handlebars/core/handlebars-loader',
		'handlebars-core' : 'shared/handlebars/core/handlebars-core',
		'handlebars-data-helpers' : 'shared/handlebars/common-helpers/data-helpers',
		'handlebars-columns-helpers' : 'shared/handlebars/common-helpers/columns-helpers',
		
		'bloomreach-mlt' : 'wcs/bloomreach/bloomreach-mlt',

		// handlebars autocomplete
		"autocomplete.base" : "shared/autocomplete/autocomplete.base",

		//search autosuggest
		"search-autosuggest" : "shared/search-autosuggest/search-autosuggest",
		"search-autosuggest-helpers": "shared/search-autosuggest/search-autosuggest-helpers"
	},
	map: {},
	"shim": {
		/* Foundation */
		'foundation.core': {
			deps: [
				'jquery',
				'modernizr',
				'matchmedia' // VFDP-637 Leverage newer matchMedia polyfill for better IE9 performance
			],
			exports: 'Foundation'
		},
		'foundation.abide':			['foundation.core'],
		'foundation.alerts':		['foundation.core'],
		'foundation.clearing':		['foundation.core'],
		'foundation.cookie':		['foundation.core'],
		'foundation.dropdown':		['foundation.core'],
		'foundation.forms':			['foundation.core'],
		'foundation.interchange':	['foundation.core'],
		'foundation.joyride':		['foundation.core','foundation.cookie'],
		'foundation.magellan':		['foundation.core'],
		'foundation.orbit':			['foundation.core'],
		'foundation.placeholder':	{
			deps: ['foundation.core'], // TODO foundation.core probably isn't necessary for placeholder
			exports: 'Placeholder'
		},
		'foundation.reveal':		['foundation.core'],
		'foundation.section':		['foundation.core'],
		'foundation.tooltips':		['foundation.core'],
		'foundation.topbar':		['foundation.core'],

		/* lib Scripts */
		'cycle': {
			deps: [
			'jquery'
			]
		},
		'ddSlick': {
			deps: [
			'jquery'
			]
		},
		'jquery.cookie': {
			deps: [
			'jquery'
			]
		},
		'jquery-lazyload':{
			deps: ['jquery']
		},
		'jquery-mobile-events': ['jquery'],
		'jquery.tooltipster': {
			deps: [
			'jquery'
			]
		},
		"jquery.validate": {
			deps: ["jquery"]
		},
		"wcs.validate-message": {
			deps: ["jquery.validate"]
		},
		'jquery-ui': {
			deps: [
			'jquery'
			]
		},
		'modernizr': {
			exports: 'Modernizr'
		},
		"ooyala":{
			deps: ["jquery"]
		},
		"owl.carousel.extended": {
			deps: ["jquery"]
		},
		'picturefill': {
			exports: 'picturefill'
		},
		"pubsub": {
			deps: ["jquery"]
		},
		/**
		 * Custom Scripts for CMS features
		 * Not needed if handled via a define() in the target script
		 **/

		// 'example': {
		//	deps: [
		//	'jquery'
		//	]
		// },

		/**
		 * Custom Scripts for WCS features
		 * Not needed if handled via a define() in the target script
		 **/
		"wcs.custom-shoes":{
			deps: ["jquery", "jquery-ui", "pubsub"]
		},
		"wcs.cm-listener": {
			deps: ["cm.eluminate"]
		},
		"wcs.gtm-listener": {
			deps: ["jquery"]
		},
		"jquery-carousel": ["jquery","jquery-mobile-events","jquery-ui"],
		"wcs.topbar-carousel":{
			deps: ["jquery-carousel","foundation.core"]
		},
		//3D Configurator Dependencies
    	"Detector":{
			deps: ["jquery"],
			exports: 'Detector'
		},
        'Tween':{
			deps: ["jquery"],
            exports: 'TWEEN'
		},
        'jqTabs':{
			deps: ["jquery"]
		},
        'OrbitControls':{
			deps: ["jquery", 'three']
		},
    	'tinycolor':{
			deps: ["jquery"]
		},
		//Isotope dependencies - style selector page
		"isotope":{
			deps: ["jquery", "jquery-bridget"]
		},
		"jquery.hoverintent":{
			deps: ["jquery"]
		}
	}
};