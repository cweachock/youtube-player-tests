
/*=== Youtube Popup ===========================================================================*/
define('youtubePopup', ["jquery"], function($) {
    function YoutubePopup(el) {
        this.setDom(el);

        this.player = null;
        this.playerSettings = {
            rel: 0,
            showinfo: 0,
            modestbranding: 1,
            theme: 'light',
            enablejsapi: 1
        };

        this.src = this.dom.el.attr('href');
        this.consoleStyles = 'background: #cc181e; color: #fff';

        this.getYoutubeKey();
        this.createPlayerHtml();
        this.bindEvents();
    }

    YoutubePopup.prototype.setDom = function(el) {
        var self = this;

        self.dom = {
            html: $('html'),
            body: $('body'),
            el: $(el)
        };
    }

    YoutubePopup.prototype.getYoutubeKey = function() {
        var self = this;
        var regex = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        self.key = self.src.match(regex) ? RegExp.$1 : false;
    }

    YoutubePopup.prototype.bindEvents = function() {
        var self = this;
        window.onYouTubeIframeAPIReady = function() {
            self.dom.body.trigger('youtube-ready');
        };

        self.dom.body.one('youtube-ready', function() {
            self.initPlayer();
        });

        self.dom.el.on('click', function(e) {
            self.play(e);
        });

        self.dom.el.on('keypress', function(e) { // On Enter/Escape/Space for accessibility
            if (e.which === 13 || e.which === 27 || e.which === 32) self.play();
        });

        self.dom.close.on('click', function() {
            self.close();
        });
    }

    YoutubePopup.prototype.createPlayerHtml = function() {
        var self = this;
        var player = '<div class="youtube-popup" id="youtube-popup-' + self.key + '"><span class="youtube-popup-mask js-youtube-popup-close"></span><button class="youtube-popup-close js-youtube-popup-close"></button><div class="youtube-popup-player"><div id="youtube-popup-iframe-' + self.key + '"></div></div></div>';

        this.dom.body.append(player);
        this.dom.player = $('#youtube-popup-' + self.key);
        this.dom.iframe = $('#youtube-popup-iframe-' + self.key);
        this.dom.close = self.dom.player.find('.js-youtube-popup-close');
    }

    YoutubePopup.prototype.initPlayer = function() {
        var self = this;

        self.player = new YT.Player('youtube-popup-iframe-' + self.key, {
            videoId: self.key,
            playerVars: self.playerSettings,
            events: {
                'onStateChange': function(e) {
                    self.onPlayerStateChange(e);
                }
            }
        });

        console.log('%c Youtube: ' + self.key + ' | Init ', self.consoleStyles); // Keep for debugging
    }

    YoutubePopup.prototype.onPlayerStateChange = function(e) {
        var self = this;

        if (e.data === 0) // Video ended
            self.close();
    }

    YoutubePopup.prototype.play = function(e) {
        var self = this;
        //console.log("playing")
        if (!this.dom.html.hasClass('mobile')) {
            e.preventDefault();

            self.dom.body.addClass('scroll-is-disable');
            self.dom.player.addClass('is-playing');
            self.player.playVideo();
            self.dom.player.one('transitionend', function() {
                self.dom.close.focus();
            }); // Focus on close btn for accessibility
        }
    }

    YoutubePopup.prototype.close = function() {
        var self = this;

        self.dom.body.removeClass('scroll-is-disable');
        self.dom.player.removeClass('is-playing');
        self.player.stopVideo();
        // self.dom.player.one('transitionend', function() {
        //     self.dom.el.focus();
        // }); // Focus on back on video preview for accessibility | Make sure to have attr tabindex="0" on the video preview.
        //disabling as was causing jump in carousel
    }

    return YoutubePopup;
});


/*=== Scroll to Y ===========================================================================*/
define('scrollToY', function() {
    // main function
    function scrollToY(el) {
        this.setDom(el);
        this.init();
        this.bindEvents();
    }

    scrollToY.prototype.setDom = function(el) {
        this.dom = {
            el: el,
            target: document.querySelector(el.getAttribute('href'))
        };
    }

    scrollToY.prototype.init = function() {
        var self = this;
        self.getScrollTargetY();
        self.speed = 500; // time in pixels per second
    }

    scrollToY.prototype.bindEvents = function() {
        var self = this;
        window.addEventListener('resize', function() { self.getScrollTargetY() });
        self.dom.el.addEventListener('click', function(e) { self.go(e) });
    }

    scrollToY.prototype.getScrollTargetY = function() {
        var self = this;
        self.scrollTargetY = self.dom.el.offsetTop; // the target scrollY property of the window
    }

    scrollToY.prototype.go = function(e) {
        e.preventDefault();
        var self = this;

        self.dom.el.blur();

        $('html, body').animate({ scrollTop: $(self.dom.target).offset().top }, self.speed);
    }

    return scrollToY;
});
