define(["jquery"], function ($){
	function initZoomViewer (params){
		var s7flyout, s7params, s7swatches;
		
		// initialize the SDK 
		s7sdk.Util.init(); 
		
		// create ParameterManager instance that will handle modifiers 
        s7params = new s7sdk.ParameterManager();
        s7params.push("serverurl", params.server);
        s7params.push("asset", params.asset);
        
		//set viewer type & version. For flyout type == "504" 
		s7params.setViewer("504,4.7.1");	
		
        // optional Swatches settings
		s7params.push("swatches.tmblayout", "1,4");
		s7params.push("swatches.tmbsize", "35,33");
		s7params.push("swatches.resizable", "1");	
		s7params.push("swatches.orientation", "1");

		var trackingManager = new s7sdk.TrackingManager();

		function initViewer(){
			s7params.push("swatches.cellspacing", "2,2");	
			s7params.push("swatches.textpos", "none");	
			if (s7sdk.browser.device.name != "desktop"){
				s7params.push("swatches.enablescrollbuttons","0");	
			}
			
            // create components
            s7flyout = new s7sdk.FlyoutZoomView("flyout", s7params);
            
            // enable event tracking for all events (default)
            trackingManager.attach(s7flyout);

			s7mediaset = new s7sdk.MediaSet(null, s7params, null);
			s7mediaset.addEventListener(s7sdk.AssetEvent.NOTF_SET_PARSED,onSetParsed, false);
		}

		function onSetParsed(e) {
			
			// set media-set on components
			s7mediasetDesc = e.s7event.asset;
			if (s7mediasetDesc.items.length > 1){
				// create Swatches component 
				s7swatches = new s7sdk.Swatches("flyout", s7params, "swatches"); 
				s7swatches.addEventListener(s7sdk.AssetEvent.SWATCH_SELECTED_EVENT, swatchSelected, false); 
				s7swatches.setMediaSet(s7mediasetDesc);
				s7swatches.selectSwatch(0, true);

			} else if (s7mediasetDesc.items.length == 1){
				s7flyout.setItem(s7mediasetDesc.items[0]);
			}
				
			if (typeof s7flyout.dynamicSize != "undefined" && s7flyout.dynamicSize == "true") 
				{
					var extraMargin = 40;
					var staticImageSize = {width:s7flyout.getComponentBounds().x+s7flyout.getComponentBounds().width,
										   height:s7flyout.getComponentBounds().y+s7flyout.getComponentBounds().height};
					var swatchesSize = {width:0,height:0};
					if(s7swatches) {
						swatchesSize = {width:s7swatches.obj.getWidth(),height:s7swatches.obj.getHeight()};
					}
					var flyoutViewSize = {width:s7flyout.flyoutView.flyoutDiv.width,height:s7flyout.flyoutView.flyoutDiv.height};
					var newWidth = staticImageSize.width + flyoutViewSize.width + extraMargin;
					var newHeight = Math.max(staticImageSize.height, flyoutViewSize.height) + swatchesSize.height + extraMargin;
					/*
					window.resizeTo(newWidth, newHeight);
					setTimeout(function(){
						window.resizeBy(newWidth-s7sdk.browser.detectScreen().w, newHeight-s7sdk.browser.detectScreen().h); 
					}
					,200);*/
				}
			}

			// change the image displayed in the main view every time the swatch selection changes 
			function swatchSelected(e) { 
				var asset = e.s7event.asset;
				if(s7flyout){
					s7flyout.setItem(asset);
				}
			} 
			
			function viewer_ASSET_CHANGED(e) { 
				if((s7swatches) && (s7swatches.frame != e.s7event.frame)){
					s7swatches.selectSwatch(e.s7event.frame, true);
				}
			} 

			s7params.addEventListener(s7sdk.Event.SDK_READY,initViewer,false);
			s7params.init();	//Initialize Parameter Manager

			//integrate SiteCatalyst logging
			//strip modifier from asset and take the very first entry from the image list, 
			//and the first element in combination from that entry
			/*
			var siteCatalystAsset = s7params.get("asset").split(',')[0].split(':')[0];
			var isConfig2Exist = false;
			if (siteCatalystAsset.indexOf('/') != -1) {
				var company = siteCatalystAsset.split('/')[0];
				var config2 = s7params.get("config2");
				isConfig2Exist = (config2 != '' && typeof config2 != "undefined");
				if (isConfig2Exist){
					document.write('<script type="text/javascript" src="../s_code.jsp?company=' + company + (config2 == '' ? '' : '&preset=' + config2) +  '"><\/script>');
				}	
			}*/

			// s7ComponentEvent function handles all the output from the SDK viewers.  The user can directly access
			// the tracking events if lower level control is desired - see UserEvent documentation.  
			//
			/*
			function s7ComponentEvent(objID, compClass, instName, timeStamp, eventData) {
				//console.log("s7ComponentEvent(" + objID + ", " + compClass + ", " + instName + ", " + timeStamp + ", " + eventData + ")");
				// s7track() passes the eventData param to SiteCatalyst tracking through s_code.jsp
				if (typeof s7track == "function"){
					s7track(eventData);
				}
			}*/
		}
	return {
		initZoomViewer: initZoomViewer
	};
});