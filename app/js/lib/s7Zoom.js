/*!************************************************************************
*
* ADOBE CONFIDENTIAL
* ___________________
*
*  Copyright 2013 Adobe Systems Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
**************************************************************************/
define (['jquery'], function (){
	//function initZoomViewer (){
		if(typeof s7viewers == "undefined") {
			s7viewers = {};
		}else if(typeof s7viewers != "object") {
			throw new Error("Cannot initialize a root 's7viewers' package. s7viewers is not an object");
		}

		if(!s7viewers.FlyoutViewer) {
			s7viewers.FlyoutViewer = function () {
				this.sdkBasePath = '/s7sdk/2.5/';
				this.containerId = null;
				this.params = {};
				this.onInitComplete = null;
				this.onInitFail = null;
			};
			s7viewers.FlyoutViewer.cssClassName = "s7flyoutviewer";
			s7viewers.FlyoutViewer.prototype.setContainerId = function (inElemId) {
				this.containerId = inElemId || null;
			};
			s7viewers.FlyoutViewer.prototype.getContentUrl = function () {
				 var contentUrl = "";
				 var viewerPath = "";
				 var scriptTags = null;
				 if (document.scripts){
					scriptTags = document.scripts;
				 }else{
					scriptTags = document.getElementsByTagName("script");
				 }
				 for(var i=0; i<scriptTags.length;i++){
					  if(scriptTags[i].getAttribute('src') && scriptTags[i].getAttribute('src').indexOf('FlyoutViewer.js') > 0){
						  viewerPath = scriptTags[i].getAttribute('src');
						  break;
					  }
				 }
				 var idx = viewerPath.indexOf('js/FlyoutViewer.js'); 
				 if (idx >= 0) {
					  contentUrl = viewerPath.substring(0,idx);
				 }
				 if ((contentUrl != '') && (contentUrl.lastIndexOf('/') != contentUrl.length - 1)) {
					 contentUrl += '/';
				 }
				return contentUrl;
			};
			s7viewers.FlyoutViewer.prototype.includeViewer = function () {
				s7sdk.Util.require("s7sdk.set.MediaSet");
				s7sdk.Util.require("s7sdk.image.FlyoutZoomView");
				s7sdk.Util.require("s7sdk.set.Swatches");
				this.trackingManager = new s7sdk.TrackingManager(); // needs to be created first to track LOAD event
				this.s7params = new s7sdk.ParameterManager(null,null,{"asset" : "MediaSet.asset"},this.getContentUrl()+"FlyoutViewer.css");
				this.s7params.setViewer("504,4.9.1");	 
				var defaultLocalizedTexts = {
				"en":{
					"FlyoutZoomView.TIP_BUBBLE_OVER":"Mouse over image for a closer look.",
					"FlyoutZoomView.TIP_BUBBLE_TAP":"Drag image to explore."
				},
				"es":{
					"FlyoutZoomView.TIP_BUBBLE_OVER":"Desliza el cursor para ver m�s de cerca.",
					"FlyoutZoomView.TIP_BUBBLE_TAP":"Arrastra la imagen para explorar."
				},
					defaultLocale: "en"
				};
				this.s7params.setLocalizedTexts(defaultLocalizedTexts);
				for(var prm in this.params){
					if (prm != "localizedtexts"){
						this.s7params.push(prm, this.params[prm]);
					}else{
						this.s7params.setLocalizedTexts(this.params[prm]);
					}
				}
				this.s7flyout = null;
				this.s7mediaset = null; 
				this.s7mediasetDesc = null; 
				this.s7visibility = null;
				this.s7swatches = null; 
				this.container = null; 
				this.initialFrame = 0;
				if (this.containerId != null){
					this.container = document.getElementById(this.containerId);
					if (this.container){
						if (this.container.className != ""){
							if (this.container.className.indexOf(s7viewers.FlyoutViewer.cssClassName) != -1){
								//
							}else{
								this.container.className += " "+s7viewers.FlyoutViewer.cssClassName;
							}	
						}else{
							this.container.className = s7viewers.FlyoutViewer.cssClassName;
						}
					}
				}
				var self = this;
				
				function initViewer(){
					self.s7params.push("tmblayout", "0,1");
					self.s7params.push("resizable", "0");	
					self.s7params.push("orientation", "0");	
	
					self.s7params.push("textpos", "none");	
					if (s7sdk.browser.device.name != "desktop"){
						self.s7params.push("enablescrollbuttons","0");	
					}
					self.s7flyout = new s7sdk.FlyoutZoomView(self.containerId, self.s7params, self.containerId+"_flyout");
					self.trackingManager.attach(self.s7flyout);
	
					self.s7mediaset = new s7sdk.MediaSet(null, self.s7params, self.containerId+"_mediaset");
					self.trackingManager.attach(self.s7mediaset);
					self.s7mediaset.addEventListener(s7sdk.AssetEvent.NOTF_SET_PARSED,onSetParsed, false);
	
					function onSetParsed(e) {
						self.s7mediasetDesc = e.s7event.asset;
						self.initialFrame = Math.max(0,parseInt((typeof(self.s7params.get('initialframe')) != 'undefined') ? self.s7params.get('initialframe') : 0));
						if (self.initialFrame < self.s7mediasetDesc.items.length){
							//
						}else{
							self.initialFrame = 0;
						}
						if (self.s7mediasetDesc.items.length > 1){
							if (self.s7swatches == null){
								self.s7swatches = new s7sdk.Swatches(self.containerId, self.s7params, self.containerId+"_swatches"); 
								self.trackingManager.attach(self.s7swatches);
								self.s7swatches.addEventListener(s7sdk.AssetEvent.SWATCH_SELECTED_EVENT, swatchSelected, false); 
							}else{
								self.s7swatches.show();
							}
							self.s7swatches.setMediaSet(self.s7mediasetDesc);
							self.s7swatches.selectSwatch(self.initialFrame, true);
	
						} else if (self.s7mediasetDesc.items.length == 1){
							if (self.s7swatches != null){
								self.s7swatches.hide(); 
							}
							self.s7flyout.setItem(self.s7mediasetDesc.items[self.initialFrame]);
						}
						
						if ((self.s7flyout.getParam("autoResize", "0").toLowerCase() == "1") ||
							(self.s7flyout.getParam("autoResize", "0").toLowerCase() == "true")
							){
							//here goes the code for window auto-resize. Unclear how to access component instances yet.
							var extraMargin = 40;
							var staticImageSize = {width:self.s7flyout.getComponentBounds().x+self.s7flyout.getComponentBounds().width,
												   height:self.s7flyout.getComponentBounds().y+self.s7flyout.getComponentBounds().height};
							var swatchesSize = {width:0,height:0};
							if(self.s7swatches) {
								swatchesSize = {width:self.s7swatches.obj.getWidth(),height:self.s7swatches.obj.getHeight()};
							}
							var flyoutViewSize = {width:self.s7flyout.flyoutView.flyoutDiv.width,height:self.s7flyout.flyoutView.flyoutDiv.height};
							var newWidth = staticImageSize.width + flyoutViewSize.width + extraMargin;
							var newHeight = Math.max(staticImageSize.height, flyoutViewSize.height) + swatchesSize.height + extraMargin;
	
							window.resizeTo(newWidth, newHeight);
							setTimeout(function(){
								window.resizeBy(newWidth-s7sdk.browser.detectScreen().w, newHeight-s7sdk.browser.detectScreen().h); 
							}
							,200);
						}
	
						if ((self.onInitComplete != null) && (typeof self.onInitComplete == "function")){
							self.onInitComplete();
						}
					}
	
					function swatchSelected(e) { 
						var asset = e.s7event.asset;
						if(self.s7flyout){
							self.s7flyout.setItem(asset);
						}
					} 
					
					function viewer_ASSET_CHANGED(e) { 
						if((self.s7swatches) && (self.s7swatches.frame != e.s7event.frame)){
							self.s7swatches.selectSwatch(e.s7event.frame, true);
						}
					} 
				}
		
		
				this.s7params.addEventListener(s7sdk.Event.SDK_READY,function(){
														self.initSiteCatalyst(self.s7params,initViewer);
												},false);
				this.s7params.init();	
			};
		
			s7viewers.FlyoutViewer.prototype.setParam = function(key, def){
				this.params[key] = def;	
			};
		
			s7viewers.FlyoutViewer.prototype.getParam = function(key){
				return this.params[key];	
			};
		
			s7viewers.FlyoutViewer.prototype.setParams = function(inParams){
				var params = inParams.split("&");
				for (var i = 0; i < params.length; i++) {
					var pair = params[i].split("=");
					if (pair.length > 1) {
						this.setParam(pair[0],decodeURIComponent(params[i].split("=")[1]));
					}
				}
			};
			
			s7viewers.FlyoutViewer.prototype.s7sdkUtilsAvailable = function(){
				return (typeof s7sdk != "undefined");
			};
		
			s7viewers.FlyoutViewer.prototype.init = function(){
				var s7sdkUtilsAddedToDOM = false;
				var utilSrcPath = this.getDomain(this.getContentUrl()) + this.sdkBasePath + "js/s7sdk/utils/Utils.js";
				var allScripts = null;
				if (document.scripts){
					allScripts = document.scripts;
				}else{
					allScripts = document.getElementsByTagName("script");
				}
				for (var i=0; i<allScripts.length; i++){ 
					if (allScripts[i] && allScripts[i].getAttribute("src")!=null && allScripts[i].getAttribute("src").indexOf(utilSrcPath)!=-1){
						s7sdkUtilsAddedToDOM = true;
						break;
					}
				}
		
				if (this.s7sdkUtilsAvailable()){
					s7sdk.Util.init(); 
					this.includeViewer();  
				}else if (!this.s7sdkUtilsAvailable() && s7sdkUtilsAddedToDOM) {
					var selfRef = this;
					var utilsWaitId = setInterval(
						function() {
							if (selfRef.s7sdkUtilsAvailable()) {
								clearInterval(utilsWaitId);
								s7sdk.Util.init(); 
								selfRef.includeViewer();  
							}
						}, 100
					);
				}else{
					var elem = document.createElement("script");
					elem.setAttribute("language", "javascript");
					elem.setAttribute("type", "text/javascript");
					elem.setAttribute("src", utilSrcPath);
		
					var elems = document.getElementsByTagName("head");
					var self = this;
					elem.onload = elem.onerror = function() {  
						if (!this.executed) { 
							this.executed = true;  
							if (self.s7sdkUtilsAvailable() && s7sdk.Util){
								s7sdk.Util.init(); 
								self.includeViewer();  
							}
						}  
					};  
		
					elem.onreadystatechange = function() {  
						var self = this;  
						if (this.readyState == "complete" || this.readyState == "loaded") {  
							setTimeout(function() { 
								self.onload(); 
								self.onreadystatechange = null
							}, 0);
						}  
					};
					elems[0].appendChild(elem);
				}
			};
		
			s7viewers.FlyoutViewer.prototype.getDomain = function(inUrl) {
				var res = /(^http[s]?:\/\/[^\/]+)/i.exec(inUrl);
				if (res == null) {
					return '';
				} else {
					return res[1];
				}
			};
		
			s7viewers.FlyoutViewer.prototype.setAsset = function(inAsset) {
				if (this.s7mediaset){
					this.s7mediaset.setAsset(inAsset);
				}else{
					this.setParam("asset", inAsset);
				}
			};
			
			s7viewers.FlyoutViewer.prototype.setLocalizedTexts = function(inText) {
				if (this.s7params){
					this.s7params.setLocalizedTexts(inText);
				}else{
					this.setParam("localizedtexts", inText);
				}
			};
		
			s7viewers.FlyoutViewer.prototype.initSiteCatalyst = function(params,inCallback) {
					// s7ComponentEvent function handles all the output from the SDK viewers.  The user can directly access
					// the tracking events if lower level control is desired - see UserEvent documentation.  
					//
					window.s7ComponentEvent = function s7ComponentEvent(objID, compClass, instName, timeStamp, eventData) {
						//console.log("s7ComponentEvent(" + objID + ", " + compClass + ", " + instName + ", " + timeStamp + ", " + eventData + ")");
						// s7track() passes the eventData param to SiteCatalyst tracking through s_code.jsp
						if (typeof s7track == "function"){
							s7track(eventData);
						}
					};
					//integrate SiteCatalyst logging
					//strip modifier from asset and take the very first entry from the image list, and the first element in combination from that entry
					var siteCatalystAsset = params.get("asset", null, "MediaSet").split(',')[0].split(':')[0];
					var isConfig2Exist = false;
					if (siteCatalystAsset.indexOf('/') != -1) {
						var company = siteCatalystAsset.split('/')[0];
						var config2 = params.get("config2");
						isConfig2Exist = (config2 != '' && typeof config2 != "undefined");
						if (isConfig2Exist){
							var jsp_src =this.getContentUrl()+'../s_code.jsp?company=' + company + (config2 == '' ? '' : '&preset=' + config2);
							var elem = document.createElement("script");
							elem.setAttribute("language", "javascript");
							elem.setAttribute("type", "text/javascript");
							elem.setAttribute("src", jsp_src);
		
							var elems = document.getElementsByTagName("head");
							var self = this;
							elem.onload = elem.onerror = function() {  
								if (!this.executed) { 
									this.executed = true;  
									if (typeof inCallback == "function"){
										inCallback();
									}
								}  
							};  
		
							elem.onreadystatechange = function() {  
								var self = this;  
								if (this.readyState == "complete" || this.readyState == "loaded") {  
									setTimeout(function() { 
										self.onload(); 
										self.onreadystatechange = null
									}, 0);
								}  
							};
							elems[0].appendChild(elem);
						}else{
							if (typeof inCallback == "function"){
								inCallback();
							}
						}	
					}
			};
		
		}
	//}
	/*return {
		initZoomViewer: initZoomViewer
	};*/
});