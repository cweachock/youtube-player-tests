/*
 * Version: 1.0.3
 * 
 * This plugin requires the following libraries:
 * jquery 1.7+
 * jquery-mobile-events
 *
 * jquery-ui is only required if easing options other than
 * linear or swing are needed.
 * 
 * In addition to the above libraries, it requires the following 
 * HTML structure to work properly:
 * <ul>
 * 	<li>
 * 		<slide container>
 * 	</li>
 * </ul>
 * 
 * The plugin should be called on the ul element
 * $('ul').carousel();
 * 
 */
(function ($){
	$.fn.carousel = function(options){
		//Settings
		var settings = $.extend({
			namespace: "ui-carousel",
			easing: "swing",
			bullets: true,
			arrowBtns: true,
			prevBtnText: "",
			nextBtnText: "",
			identifier: "",
			autoRotate: false,
			endless: true,
			responsive: true,
			maxWidth : undefined,
			engine: "jquery",
			animation: "slide",
			speed: 1150
		}, options);
		//Private Attributes
		var self = this,
			$wrapper = null,
			$slides = null,
			$bullets = null,
			$firstSlide = null,
			activeSlideIndex = 0,
			slideContainerWidth = 0,
			slideWidth = 0,
			totalSlides = 0,
			upperBound = 0,
			isAnimated = false,
			animation = null;
		//Private Animations
		var slideAnimation = function () {
		    this.next = function ($current, $next) {
		    	isAnimated = true;
			    $next.css('zIndex', '4').addClass(settings.namespace + '-active');
			    $current.animate({marginLeft: '-100%'}, settings.speed, settings.easing);
			    $next.animate({marginLeft: '0%'}, settings.speed, settings.easing, function (){
			    	$current.css('marginLeft', '100%');
			    	isAnimated = false;
			    });
		    };
		    this.prev = function ($current, $prev) {
		    	isAnimated = true;
			    $prev.css('zIndex', '4').addClass(settings.namespace + '-active');
			    $current.animate({marginLeft: '100%'}, settings.speed, settings.easing);
			    $prev.css('marginLeft', '-100%');
			    $prev.animate({marginLeft: '0%'}, settings.speed, settings.easing, function (){
			    	isAnimated = false;
			    });

		    };
		},
		rowAnimation = function (){
			this.next = function ($container, $current, $next){
				var nextLeftValue = parseInt ($container.css ('left')) - slideWidth;
				isAnimated = true;
				$next.addClass (settings.namespace + '-active');
				$container.animate ({left: '-=' + slideWidth}, settings.speed, settings.easing, function (){
					isAnimated = false;
				});
				/*if (!settings.endless && nextLeftValue > upperBound){
					$next.addClass (settings.namespace + '-active');
					$container.animate ({left: '-=' + slideWidth}, settings.speed, settings.easing, function (){
						isAnimated = false;
					});
				}
				else{
					isAnimated = false;
				}*/
			};
			this.prev = function ($container, $current, $prev){
				var nextLeftValue = parseInt ($container.css ('left')) + slideWidth;
				isAnimated = true;
				$prev.addClass(settings.namespace + '-active');
				$container.animate ({left: '+=' + slideWidth}, settings.speed, settings.easing, function (){
					isAnimated = false;
				});
				/*if (!settings.endless && nextLeftValue <= 0){
					$container.animate ({left: '+=' + slideWidth}, settings.speed, settings.easing, function (){
						isAnimated = false;
					});
				}
				else{
					isAnimated = false;
				}*/
			};
		};
		//Private Functions
		//Initialize Carousel
		self.init = function ($container){
			self.createMarkUp($container);
			if (settings.animation === "slide"){
				animation = new slideAnimation($container);
			}
			else if (settings.animation === "row"){
				animation = new rowAnimation ();
			}
		};
				
		//Create Bullets
		self.generateBullets = function (totalItems){
			var bullets = '';
			bullets += '<div class="' + settings.namespace + '-bullets-container">';
			bullets += '<ol class="' + settings.namespace + '-bullets ' + settings.namespace + '-controls">';
			bullets += '<li class="' + settings.namespace + '-active" data-bullet-index="0"></li>';
			for (var i = 1; i < totalItems; i++){
				bullets += '<li data-bullet-index="' + i + '"></li>';
			}
			bullets += '</ol></div>';
			return bullets;
		};
		//Creates the required markup
		self.createMarkUp = function ($container){
			$container.wrap('<div class="' + settings.namespace + '-container ' + settings.identifier + '"></div>');
			$wrapper = $container.parents('.' + settings.namespace + '-container');
			$slides = $container.find('> li');
			$firstSlide = $slides.eq(0).addClass(settings.namespace + '-active');
			if (settings.animation === 'row'){
				slideWidth = $slides.eq(0).width ();
				slideContainerWidth = slideWidth * $slides.length + 20;
				totalSlides = $slides.length;
				upperBound = -1 * (totalSlides * slideWidth);
				$container.width (slideContainerWidth);
			}
			else{
				$slides.css({marginLeft: '100%', marginRight: '0', zIndex: '2'});
				//$firstSlide = $slides.eq(0).css({marginLeft: '0', marginRight: '0', zIndex: '4'}).addClass(settings.namespace + '-active');
				$firstSlide.css({marginLeft: '0', marginRight: '0', zIndex: '4'});
			}
			//Enable arrow butons
			if (settings.arrowBtns){
				$wrapper.append('<div class="' + settings.namespace + '-prev ' + settings.namespace + '-controls">'+
								'<span>' + settings.prevBtnText + '</span></div>'+
							'<div class="' + settings.namespace + '-next ' + settings.namespace + '-controls">'+
								'<span>' + settings.nextBtnText + '</span></div>');
			}
			//Enable bullets
			if (settings.bullets){
				$wrapper.append (self.generateBullets ($slides.length));
				$bullets = $wrapper.find('.' + settings.namespace + '-bullets');
			}
		};
		//Resets the bullet state
		self.resetBulletState = function (){
			var $bulletList = $bullets.find ('li');
			$bulletList.removeClass (settings.namespace + '-active');
			$bulletList.eq (0).addClass (settings.namespace + '-active');
			$slides.removeClass (settings.namespace + '-active');
			$firstSlide.addClass (settings.namespace + '-active');
		};
		//Updates the bullets based on the active slide
		self.updateBullets = function (){
			var activeClass = settings.namespace + '-active';
			$bullets.find ('.' + activeClass).removeClass (activeClass);
			$bullets.find ('li[data-bullet-index="' + activeSlideIndex + '"]').addClass (activeClass);
		};
		//Updates the container height based on the viewport dimensions
		self.updateContainerHeight = function (){
			var $current = $firstSlide,
				updatedHeight = $current.height ();
			$wrapper.find ('.' + settings.namespace + '-next').height (updatedHeight);
			$wrapper.find ('.' + settings.namespace + '-prev').height (updatedHeight);
	      	$wrapper.find ('> ul').height (updatedHeight);
		};
		//Updates the slides margins based on the viewport dimensions
		self.updateSlidesMargins = function (){
			if (settings.maxWidth){
				if ($(window).width() >= settings.maxWidth || !settings.endless){
		      		//$slides.css('margin', '0');
		      		$slides.removeAttr ('style');
		      		$slides.parent ().removeAttr ('style');
		      	}
				else{
					$slides.each (function (){
						if ($(this).hasClass (settings.namespace + '-active')){
							$(this).css({marginLeft: '0', marginRight: '0'});
						}
						else{
							$(this).css({marginLeft: '100%', marginRight: '0'});
						}
					});
				}
			}
		};
		//Updates the container dimensions and the slides margins based on the viewport dimensions
		self.updateContainer = function (){
			//Enable Resizing
			if (settings.animation !== 'row'){
				if (settings.responsive){
					self.updateContainerHeight();
				}
				self.updateSlidesMargins();
			}
			else{
				self.css ('left', 0);
				self.resetBulletState ();
				slideWidth = $slides.eq(0).width ();
				slideContainerWidth = $slides.length * slideWidth + 20;
				self.css ('width', slideContainerWidth);
				//if ($(window).width () > settings.maxWidth){}
			}
		};
		//Moves to Selected Slide
		self.goToSlide = function (index, $container){
			var activeClass = settings.namespace + '-active',
				activeBulletIndex = $bullets.find ('.' + activeClass).data ('bullet-index'),
				$current = $wrapper.find('ul .' + activeClass),
				$selectedSlide = $slides.eq (index);
			if (index > activeBulletIndex){
				activeSlideIndex = index - 1;
				self.nextSlide ($container, $current, $selectedSlide);
			}
			else if (index < activeBulletIndex){
				activeSlideIndex = index + 1;
				self.prevSlide ($container, $current, $selectedSlide);
			}
		};
		//Move to Next Slide
		self.nextSlide = function ($container, $current, $next){
			if (isAnimated || (settings.maxWidth !== undefined && $(window).width() > settings.maxWidth)) {return false;}
			if ((settings.endless) || (!settings.endless && activeSlideIndex < totalSlides - 1)){
				$current = $current || $container.find('.' + settings.namespace + '-active');
				$next = $next || $current.next();
				if ($next.length < 1){
					$next = $container.find('li').eq(0);
					activeSlideIndex = 0;
				}
				else{
					activeSlideIndex += 1;
				}
				$current.removeClass(settings.namespace + '-active');
				if (settings.animation == "slide"){
					$current.css('zIndex', '2');
					animation.next($current, $next);
				}
				else if (settings.animation == "row"){
					animation.next ($container, $current, $next);
				}
				if (settings.bullets){
					self.updateBullets();
				}
			}
		};
		//Move to Previous Slide
		self.prevSlide = function ($container, $current, $prev){
			if (isAnimated || (settings.maxWidth !== undefined && $(window).width() > settings.maxWidth)) {return false;}
			if ((settings.endless) || (!settings.endless && activeSlideIndex > 0)){
				$current = $current || $container.find('.' + settings.namespace + '-active');
				$prev = $prev || $current.prev();
				if ($prev.length < 1){
	    			$prev = $slides.eq($slides.length - 1);
	    			activeSlideIndex = $slides.length - 1;
	    		}
	    		else{
	    			activeSlideIndex -= 1;
	    		}
	    		$current.removeClass(settings.namespace + '-active');
				if (settings.animation == "slide"){
		    		$current.css('zIndex', '2');
					animation.prev($current, $prev);
				}
				else if (settings.animation == "row"){
					animation.prev ($container, $current, $prev);
				}
				if (settings.bullets){
					self.updateBullets();
				}
			}
		};
		//Defenition
		return this.each( function (){
			var $ul = $(this);
			//Contruct Widget
			self.init($ul);
			//Register Swipe Event Handlers
			$wrapper.on({
				swiperight: function (event){
					self.prevSlide ($ul);
				},
				swipeleft: function (event){
					self.nextSlide ($ul);
				}
			});
			//Register Click Event Handlers for Next and Previous
			$wrapper.find ('.' + settings.namespace + '-prev').on ({
				click: function (event){
					event.preventDefault();
					self.prevSlide($ul);
				}
			});
			$wrapper.find ('.' + settings.namespace + '-next').on ({
				click: function (event){
					event.preventDefault();
					self.nextSlide($ul);
				}
			});
			//Register Click Event Handlers for Bullets
			$wrapper.find ('.' + settings.namespace + '-bullets').on ({
				click: function (event){
					var $this = $(event.target);
					event.preventDefault();
					self.goToSlide ($this.data ('bullet-index'), $ul);
				}
			});
			//Register Window Event Handlers
			$(window).on({
				resize: self.updateContainer,
				load: self.updateContainer
			});
		});
	};
})(jQuery);
