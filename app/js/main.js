    var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


      // create variables for videos 
      var duffle;
      var mountain;
      var subway;
      var venue;

      //create the video variables with specific id's and event data
      function onYouTubeIframeAPIReady() {
        
              duffle = new YT.Player('duffle', {
              height: '400',
              width: '100%',
              videoId: 'ywFjA7qbMk0',
              playerVars: { 
                rel:0, 
                modestbranding: true,
                controls:0
              },
              events: {
                'onStateChange': onDuffleStateChange
              }
              
            });

        mountain = new YT.Player('mountain', {
          height: '400',
          width: '100%',
          videoId: '6ic1NMoYLV4',
          playerVars: { 
                rel:0, 
                modestbranding: true,
                controls:0
              },
          events: {
                'onStateChange': onMountainStateChange
              }
        });

        subway = new YT.Player('subway', {
          height: '400',
          width: '100%',
          videoId: 'aSi6QEGgw9E',
          playerVars: { 
                rel:0, 
                modestbranding: true,
                controls:0
              },
          events: {
                'onStateChange': onSubwayStateChange
              }
        });

        venue = new YT.Player('venue', {
          height: '400',
          width: '100%',
          videoId: 'VavyWUUcLQo',
          playerVars: { 
                rel:0, 
                modestbranding: true,
                controls:0
              },
          events: {
                'onStateChange': onVenueStateChange
              }
        });

      }

      function playDuffle(){
        duffle.playVideo();
        document.getElementById("duffle").classList.remove("fadeOut");
        document.getElementById("duffle").classList.add("fadeIn");
        
      }

      /*function closeVideo(){
        duffle.stopVideo();
        mountain.stopVideo();
        subway.stopVideo();
        venue.stopVideo();
        document.getElementById("duffle").classList.remove("fadeIn");
        document.getElementById("duffle").classList.add("fadeOut");
        document.getElementById("mountain").classList.remove("fadeIn");
        document.getElementById("mountain").classList.add("fadeOut");
        document.getElementById("subway").classList.remove("fadeIn");
        document.getElementById("subway").classList.add("fadeOut");
        document.getElementById("venue").classList.remove("fadeIn");
        document.getElementById("venue").classList.add("fadeOut");        
      }*/

      function playMountain(){
        mountain.playVideo();  
        document.getElementById("mountain").classList.remove("fadeOut");
        document.getElementById("mountain").classList.add("fadeIn");
           
      }

      function playSubway(){
        subway.playVideo();
        document.getElementById("subway").classList.remove("fadeOut");
        document.getElementById("subway").classList.add("fadeIn");
              
      }

      function playVenue(){
        venue.playVideo(); 
        document.getElementById("venue").classList.remove("fadeOut");
        document.getElementById("venue").classList.add("fadeIn");
              
      }

       //on duffle video player state change do something
      function onDuffleStateChange(event){

        //playing functionality: fades out thumb image and plays video
        if (event.data === YT.PlayerState.PLAYING){
          document.getElementById("duffle").classList.add("fadeIn");
          document.getElementById("duffle").classList.remove("fadeOut");
          dufflePosterImg.setAttribute("style", "z-index:1");
          mountain.pauseVideo();
          subway.pauseVideo();
          venue.pauseVideo();
        }

        //paused functionality: fades in thumb image and pauses video
        if(event.data === YT.PlayerState.PAUSED){
          console.log("video paused");
          document.getElementById("duffle").classList.add("fadeOut");
          document.getElementById("duffle").classList.remove("fadeIn");
          document.getElementById("youtubeBranding").classList.add("fadeOut");
          document.getElementById("youtubeBranding").classList.remove("fadeIn");
          dufflePosterImg.classList.add("fadeIn");
          dufflePosterImg.setAttribute("style", "z-index:2");
          closeIcon.setAttribute("style", "display:none;");


        }

        //video completed: fades in thumb image 
        if(event.data === YT.PlayerState.ENDED){
          console.log("video stopped");
          document.getElementById("duffle").classList.remove("fadeIn");
          document.getElementById("duffle").classList.add("fadeOut");
          closeIcon.classList.add("fadeOut");
          dufflePosterImg.classList.add("fadeIn");
          dufflePosterImg.setAttribute("style", "z-index:2");

        }

      }

      //on mountain video player state change do something
      function onMountainStateChange(event){

        //playing functionality: fades out thumb image and plays video
        if (event.data === YT.PlayerState.PLAYING){
          document.getElementById("mountain").classList.remove("fadeOut");
          document.getElementById("mountain").classList.add("fadeIn");
          mountainPosterImg.setAttribute("style", "z-index:1");
          duffle.pauseVideo();
          subway.pauseVideo();
          venue.pauseVideo();

        }

        //paused functionality: fades in thumb image and pauses video
        if(event.data === YT.PlayerState.PAUSED){
          console.log("video paused");
          document.getElementById("mountain").classList.add("fadeOut");
          document.getElementById("mountain").classList.remove("fadeIn");
          mountainPosterImg.classList.add("fadeIn");
          mountainPosterImg.setAttribute("style", "z-index:2");
        }

        //video completed: fades in thumb image 
        if(event.data === YT.PlayerState.ENDED){
          console.log("video stopped");
          document.getElementById("mountain").classList.remove("fadeOut");
          document.getElementById("mountain").classList.add("fadeIn");
          mountainPosterImg.classList.add("fadeIn");
          mountainPosterImg.setAttribute("style", "z-index:2");
        }
      }

      //on subway video player state change do something
      function onSubwayStateChange(event){

        //playing functionality: fades out thumb image and plays video
        if (event.data === YT.PlayerState.PLAYING){
          document.getElementById("subway").classList.remove("fadeOut");
          document.getElementById("subway").classList.add("fadeIn");
          subwayPosterImg.setAttribute("style", "z-index:1");
          mountain.pauseVideo();
          duffle.pauseVideo();
          venue.pauseVideo();
        }

        //paused functionality: fades in thumb image and pauses video
        if(event.data === YT.PlayerState.PAUSED){
          console.log("video paused");
          document.getElementById("subway").classList.add("fadeOut");
          document.getElementById("subway").classList.remove("fadeIn");
          subwayPosterImg.classList.add("fadeIn");
          subwayPosterImg.setAttribute("style", "z-index:2");
        }

        //video completed: fades in thumb image 
        if(event.data === YT.PlayerState.ENDED){
          console.log("video stopped");
          document.getElementById("subway").classList.remove("fadeOut");
          document.getElementById("subway").classList.add("fadeIn");
          subwayPosterImg.classList.add("fadeIn");
          subwayPosterImg.setAttribute("style", "z-index:2");
        }
      }

      //on subway video player state change do something
      function onVenueStateChange(event){

        //playing functionality: fades out thumb image and plays video
        if (event.data === YT.PlayerState.PLAYING){
          document.getElementById("venue").classList.remove("fadeOut");
          document.getElementById("venue").classList.add("fadeIn");
          venuePosterImg.setAttribute("style", "z-index:1");
          mountain.pauseVideo();
          subway.pauseVideo();
          duffle.pauseVideo();
        }

        //paused functionality: fades in thumb image and pauses video
        if(event.data === YT.PlayerState.PAUSED){
          console.log("video paused");
          document.getElementById("venue").classList.add("fadeOut");
          document.getElementById("venue").classList.remove("fadeIn");
          venuePosterImg.classList.add("fadeIn");
          venuePosterImg.setAttribute("style", "z-index:2");
        }

        //video completed: fades in thumb image 
        if(event.data === YT.PlayerState.ENDED){
          console.log("video stopped");
          document.getElementById("venue").classList.remove("fadeOut");
          document.getElementById("venue").classList.add("fadeIn");
          venuePosterImg.classList.add("fadeIn");
          venuePosterImg.setAttribute("style", "z-index:2");
        }
      }





require(['jquery', 'domReady'],function($, domReady){domReady(function(){


var mountainThumb = document.getElementById("mountain");
var dufflePosterImg = document.getElementById("dufflePosterImg");
var mountainPosterImg = document.getElementById("mountainPosterImg");
var subwayPosterImg = document.getElementById("subwayPosterImg");
var venuePosterImg = document.getElementById("venuePosterImg");


dufflePosterImg.addEventListener("click", playDuffle, false);
mountainPosterImg.addEventListener("click", playMountain, false);
subwayPosterImg.addEventListener("click", playSubway, false);
venuePosterImg.addEventListener("click", playVenue, false);
closeIcon.addEventListener("click", closeVideo, false);

    });
});
